﻿using System;
using System.Threading.Tasks;

namespace PS.BLL.Data
{
    public interface IUow : IDisposable
    {
        IRepository<Person> Persons { get; }

        Task SaveChanges();
    }
}
