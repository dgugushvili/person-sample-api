﻿using PS.BLL.Data;

namespace PS.BLL.Services
{
    public class BaseService 
    {
        protected readonly IUow _uow;

        public BaseService(IUow uow)
        {
            _uow = uow;
        }
    }
}
