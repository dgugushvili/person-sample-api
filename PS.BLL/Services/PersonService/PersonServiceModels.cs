﻿using PS.BLL.Models;
using System;

namespace PS.BLL.Services
{
    public class PersonM
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BirthDate { get; set; }
    }

    public class PersonListFilterM : BaseListFilterM
    {
    }

    public class PersonListVM : BaseListVM<PersonVM>
    {
    }

    public class PersonVM
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
