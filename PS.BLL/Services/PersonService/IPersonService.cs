﻿using PS.BLL.Models;
using System.Threading.Tasks;

namespace PS.BLL.Services
{
    public interface IPersonService
    {
        ResultVM<PersonListVM> GetPersonList(PersonListFilterM model);
        ResultVM<PersonVM> GetPerson(int id);
        Task<ResultVM<bool>> EditPerson(PersonM model);
        Task<ResultVM<bool>> DeletePerson(int id);
    }
}
