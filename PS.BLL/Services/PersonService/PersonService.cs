﻿using PS.BLL.Data;
using PS.BLL.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PS.BLL.Services
{
    public class PersonService : BaseService, IPersonService
    {
        public PersonService(IUow uow) : base(uow) { }

        public ResultVM<PersonListVM> GetPersonList(PersonListFilterM model)
        {
            var result = new ResultVM<PersonListVM>();

            try
            {
                result.Value = new PersonListVM
                {
                    Data = _uow.Persons.GetAll().Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize)
                        .Select(x => new PersonVM
                        {
                            Id = x.Id,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            PersonalNumber = x.PersonalNumber,
                            PhoneNumber = x.PhoneNumber,
                            BirthDate = x.BirthDate
                        }).ToList(),
                    TotalCount = _uow.Persons.GetAll().Count()
                };
            }
            catch (Exception ex)
            {
                result.AddNotification(ex);
            }

            return result;
        }

        public ResultVM<PersonVM> GetPerson(int id)
        {
            var result = new ResultVM<PersonVM>();

            try
            {
                result.Value = _uow.Persons.GetAll().Select(x => new PersonVM
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    PersonalNumber = x.PersonalNumber,
                    PhoneNumber = x.PhoneNumber,
                    BirthDate = x.BirthDate
                }).FirstOrDefault(x => x.Id == id)
                ?? throw new CustomException("Person not found");
            }
            catch (Exception ex)
            {
                result.AddNotification(ex);
            }

            return result;
        }

        public async Task<ResultVM<bool>> EditPerson(PersonM model)
        {
            var result = new ResultVM<bool>();

            try
            {
                if (model.Id == null)
                {
                    _uow.Persons.Add(new Person
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PersonalNumber = model.PersonalNumber,
                        PhoneNumber = model.PhoneNumber,
                        BirthDate = model.BirthDate,
                        CreateDate = DateTime.Now
                    });
                }
                else
                {
                    var person = _uow.Persons.GetAll().FirstOrDefault(x => x.Id == model.Id.Value)
                        ?? throw new CustomException("Person not found");

                    person.FirstName = model.FirstName;
                    person.LastName = model.LastName;
                    person.PersonalNumber = model.PersonalNumber;
                    person.PhoneNumber = model.PhoneNumber;
                    person.BirthDate = model.BirthDate;
                    person.LastModifyDate = DateTime.Now;
                }

                await _uow.SaveChanges();

                result.Value = true;
            }
            catch (Exception ex)
            {
                result.AddNotification(ex);
            }

            return result;
        }

        public async Task<ResultVM<bool>> DeletePerson(int id)
        {
            var result = new ResultVM<bool>();

            try
            {
                var person = _uow.Persons.GetAll().FirstOrDefault(x => x.Id == id)
                    ?? throw new CustomException("Person not found");

                _uow.Persons.Remove(person);

                await _uow.SaveChanges();

                result.Value = true;
            }
            catch (Exception ex)
            {
                result.AddNotification(ex);
            }

            return result;
        }
    }
}
