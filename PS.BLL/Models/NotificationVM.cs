﻿namespace PS.BLL.Models
{
    public class NotificationVM
    {
        public NotificationType Type { get; set; }
        public string Message { get; set; }
    }
}
