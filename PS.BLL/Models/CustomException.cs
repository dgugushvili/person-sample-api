﻿using System;

namespace PS.BLL.Models
{
    public class CustomException : Exception
    {
        public CustomException(string message)
        {
            Message = message;
        }

        public new string Message { get; set; }
    }
}
