﻿using System;
using System.Collections.Generic;

namespace PS.BLL.Models
{
    public class ResultVM<T>
    {
        public T Value { get; set; }

        public List<NotificationVM> Notifications { get; set; } = new List<NotificationVM>();

        internal void AddNotification(NotificationType type, string message)
        {
            Notifications.Add(new NotificationVM
            {
                Type = type,
                Message = message
            });
        }

        internal void AddNotification(Exception ex)
        {
            if (ex is CustomException)
            {
                AddNotification(NotificationType.Error, (ex as CustomException).Message);
            }
            else
            {
                AddNotification(NotificationType.Error, "Internal server error");
            }
        }
    }
}
