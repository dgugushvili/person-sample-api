﻿using System.Collections.Generic;

namespace PS.BLL.Models
{
    public class BaseListFilterM
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class BaseListVM<T>
    {
        public int TotalCount { get; set; }
        public List<T> Data { get; set; }
    }
}
