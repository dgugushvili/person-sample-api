﻿using Microsoft.EntityFrameworkCore;
using PS.BLL.Data;

namespace PS.DAL
{
    public class PSDbContext : DbContext
    {
        public PSDbContext(DbContextOptions<PSDbContext> options)
            : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");

            builder.Entity<Person>().ToTable("Person");
            builder.Entity<Person>().HasKey(x => x.Id);

            base.OnModelCreating(builder);
        }
    }
}
