﻿using Microsoft.EntityFrameworkCore;
using PS.BLL.Data;
using System.Threading.Tasks;

namespace PS.DAL
{
    public class Uow : IUow
    {
        private PSDbContext _dbContext;

        public Uow(PSDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IRepository<Person> Persons => new Repository<Person>(_dbContext);

        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
