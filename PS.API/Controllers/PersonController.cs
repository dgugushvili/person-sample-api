﻿using Microsoft.AspNetCore.Mvc;
using PS.BLL.Models;
using PS.BLL.Services;
using System.Threading.Tasks;

namespace PS.API.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [ApiController]
    public class PersonController
    {
        public readonly IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpPost]
        public ResultVM<PersonListVM> GetPersonList(PersonListFilterM model)
        {
            return _personService.GetPersonList(model);
        }

        public ResultVM<PersonVM> GetPerson(int id)
        {
            return _personService.GetPerson(id);
        }

        [HttpPost]
        public async Task<ResultVM<bool>> EditPerson(PersonM model)
        {
            return await _personService.EditPerson(model);
        }

        [HttpPost]
        public async Task<ResultVM<bool>> DeletePerson(int id)
        {
            return await _personService.DeletePerson(id);
        }
    }
}
